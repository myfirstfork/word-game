#include<cstdlib>
#include<iostream>
#include<fstream>
#include<vector>
#include<ctime>
#include<windows.h>
#include"Trie.cpp"

using namespace std;

int gsize=0;

struct HighScore
{
	int gridSize,score;
	string name,curtime;
};

vector<vector<char> > createGrid(int x, int gsize)
{
	int i,j;
	char p;
	vector<vector<char> > s;
	srand(x);
	for(i=0;i<gsize;i++)
	{
		vector<char> c;
		for(j=0;j<gsize;j++)
		{
			p = (char)(rand()%26 + 'a');
			c.push_back(p);
		}
		s.push_back(c);
	}
	return s;
}

bool findWordsUtil(vector<vector<char> > grid, bool visited[][100], int i, int j, string &str, string word)
{
    visited[i][j] = true;
    str = str + grid[i][j];
 	//cout<<str<<" "<<word<<endl;
 	//Sleep(1000);
	if(str[str.size()-1] != word[str.size()-1])
 	{
 		str.erase(str.length()-1);
    	visited[i][j] = false;
 		return false;
 	}
 	
    if (str.compare(word)==0)
    {
    	return true;
    }
 
    for (int row=i-1; row<=i+1 && row<gsize; row++)
    {
    	for (int col=j-1; col<=j+1 && col<gsize; col++)
    	{
    		if (row>=0 && col>=0 && !visited[row][col])
    		{
    			if(findWordsUtil(grid, visited, row, col, str, word))
    			{
    				return true;
    			}
    		}
    	}
    }
    str.erase(str.length()-1);
    visited[i][j] = false;
	return false;
}
 
bool findWord(vector<vector<char> > grid, string word)
{
	int i,j;
    bool visited[100][100]={{false}};
    string str = "";
 
    for (int i=0; i<gsize; i++)
    {
    	for (int j=0; j<gsize; j++)
    	{
    		if(grid[i][j] == word[0])
    		{
    			if(findWordsUtil(grid, visited, i, j, str,word))
    			{
    				return true;
    			}
    		}
    	}
    }    
    return false;
}

void printGrid(vector<vector<char> > grid, int gsize, int score)
{
	int i,j;
	cout<<"----------------------------------------------------------"<<endl;
	for(i=0;i<gsize;i++)
	{
		cout<<"\t";
		for(j=0;j<gsize;j++)
		{
			cout<<grid[i][j]<<" ";
		}
		cout<<endl;
	}
	cout<<"----------------------------------------------------------"<<endl;
	cout<<"Enter The Word (Enter 0 to Exit!)\tSCORE : "<<score<<endl;
	return;
}

void updateScore(struct HighScore hs)
{
	ofstream ofs("HighScore.txt",ios::app);
	ofs<<hs.gridSize<<'\n'<<hs.name<<'\n'<<hs.score<<'\n'<<hs.curtime<<endl;
	return;
}


void displayHighScore()
{
	struct HighScore hs;
	ifstream ifs("HighScore.txt");
	while(ifs>>hs.gridSize>>hs.name>>hs.score>>hs.curtime)
	{
		getline(ifs,hs.curtime);
		cout<<hs.gridSize<<"\t"<<hs.name<<"\t"<<hs.score<<"\t"<<hs.curtime<<endl;
	}
	return;
}

int main()
{
	Trie ob;
	int co=0;
	ifstream ifob("Dictionary.txt");
	string s;
	while(ifob>>s)
	{
		if(s.size()>2)
		{
			ob.insert(s);
			co++;
		}
		
	}
	cout<<co<<endl;
	int x = time(NULL);
	int i,j,ch,score;
	vector<vector<char> > grid;
	string word;
	struct HighScore hs;
	
	HANDLE hc;
	hc = GetStdHandle(STD_OUTPUT_HANDLE);
	
	while(1)
	{
		SetConsoleTextAttribute(hc, 12);
		cout<<"---------------------------------------"<<endl;
		cout<<"\tLord of Words"<<endl;
		cout<<"---------------------------------------"<<endl;
		SetConsoleTextAttribute(hc, 10);
		cout<<"1. Start Game"<<endl;
		cout<<"2. Check High Score"<<endl;
		cout<<"3. Exit"<<endl;
		cout<<"---------------------------------------"<<endl;
		cout<<"Enter Your Choice"<<endl;
		cin>>ch;
		time_t now = time(0);
		if(ch==1)
		{
			cout<<"Choose the size of the grid"<<endl;
			cin>>gsize;
			while(gsize<4 || gsize>20)
			{
				cout<<"Invalid Grid Size!\nMust be between 4 and 20."<<endl;
				cin>>gsize;
			}
			
			
			system("CLS");
			score = 0;
			
			grid = createGrid(x, gsize);
			vector<string> fwords;
			printGrid(grid,gsize,score);
			cin>>word;
			while(word.compare("0")!=0)
			{
				if(findWord(grid, word))
				{
					if(ob.search(word))
					{
						score = score + 10*(word.size());
						cout<<"You got "<<word.size()*10<<" more points"<<endl;
						cout<<"Your score is "<<score<<endl;	
						fwords.push_back(word);
						ob.deleteNode(word);
					}
					else
					{
						cout<<"Incorrect Word!"<<endl;
					}	
				}
				else
				{
					cout<<"Not Present in Grid"<<endl;
				}
				Sleep(1500);
				system("CLS");
				printGrid(grid,gsize,score);
				cin>>word;
			}
			system("CLS");
			fflush(stdin);
			
			cout<<"--------------------------------"<<endl;
			for(i=0;i<fwords.size();i++)
			{
				cout<<fwords[i]<<endl;
				ob.insert(fwords[i]);
			}
			cout<<"--------------------------------"<<endl;
			
			cout<<"Enter you name : ";
			string pname;
			fflush(stdin);
			getline(cin,pname);
			hs.name = pname;
			hs.score = score;
			fflush(stdin);
			hs.curtime = ctime(&now);
			hs.gridSize = gsize;
			updateScore(hs);
			system("CLS");
		}
		else if(ch==2)
		{
			system("CLS");
			cout<<"Size\tName\tScore\tDate & Time"<<endl;
			cout<<"-------------------------------------------------------------"<<endl;
			displayHighScore();
			cout<<"-------------------------------------------------------------"<<endl;
			fflush(stdin);
			getchar();
			system("CLS");
		}
		else if(ch==3)
		{
			system("CLS");
			cout<<"Thank You For Playing This Game"<<endl;
			Sleep(500);
			exit(0);
		}		
		else
		{
			system("CLS");
			cout<<"Invalid Choice!!!";
			Sleep(1500);
			system("CLS");
		}
	}
	return 0;
}
